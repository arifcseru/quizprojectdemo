package app.vehiclemanagement.presentationtier;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import app.vehiclemanagement.presentationtier.forms.*;
import app.vehiclemanagement.businesstier.CustomerTO;
import app.vehiclemanagement.persistencetier.CustomerService;

public class ApplicationMain {
	public static boolean isLogin = false;

	public static void main(String[] a) {
		JFrame application = new JFrame(">>>>>>>>>>>>>>>>>Vehicle Garrage Management System<<<<<<<<<<<<<<");
		application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		application.add(new ApplicationNavigationScreen());
		application.setSize(800, 500);
		application.setLocation(30, 20);

		application.setVisible(false);
		application.setUndecorated(true);
		application.setResizable(false);

		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		GraphicsDevice device = ge.getDefaultScreenDevice();

		device.setFullScreenWindow(application);
		application.setVisible(true);
	}
}

class ApplicationNavigationScreen extends JPanel implements ActionListener {
	JTextField navigationTf;
	JPanel addCustomerPanel = null;
	JPanel allCustomersPanel = null;
	JPanel parkedVehiclesPanel = null;
	JPanel parkVehicleNowPanel = null;
	JPanel unParkVehicleNowPanel = null;
	JPanel dischargedVehiclesPanel = null;
	JPanel helpPanel = null;
	JPanel footerPanel = null;
	JPanel loginPanel = null;
	JTextField userNameTf = null;
	JPasswordField passwordTf = null;

	public ApplicationNavigationScreen() {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				public void run() {
					makeGUI();
				}
			});
		} catch (Exception exc) {
			System.out.println("Can't create because of " + exc);
		}
	}

	private void makeGUI() {
		setLayout(new FlowLayout());

		JLabel applicationName = new JLabel(">>>>>>>>>>>>>>>>>>>>>>>>Vehicle Garrage Management System<<<<<<<<<<<<<<");
		applicationName.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
		add(applicationName);

		ImageIcon add_customer = new ImageIcon("resources/images/add_customer.gif");
		JButton button = new JButton(add_customer);
		button.setText("Add Customer");
		button.setSize(50, 30);
		button.setActionCommand("addCustomerView");
		button.addActionListener(this);
		add(button);

		ImageIcon user = new ImageIcon("resources/images/customers_icon.gif");
		button = new JButton(user);
		button.setText("All Customers");
		button.setSize(50, 30);
		button.setActionCommand("showAllCustomersView");
		button.addActionListener(this);
		add(button);

		ImageIcon park_icon = new ImageIcon("resources/images/park_icon.gif");
		button = new JButton(park_icon);
		button.setText("Park Vehicle");
		button.setSize(50, 30);
		button.setActionCommand("parkVehicleNowView");
		button.addActionListener(this);
		add(button);

		ImageIcon vehicle = new ImageIcon("resources/images/vehicle_icon.gif");
		button = new JButton(vehicle);
		button.setText("Parked Vehicles");
		button.setSize(50, 30);
		button.setActionCommand("showParkedVehiclesView");
		button.addActionListener(this);
		add(button);

		ImageIcon un_park_icon = new ImageIcon("resources/images/unpark_icon.gif");
		button = new JButton(un_park_icon);
		button.setText("Discharge Vehicle");
		button.setSize(50, 30);
		button.setActionCommand("unParkVehicleNowView");
		button.addActionListener(this);
		add(button);

		ImageIcon discharge_icon = new ImageIcon("resources/images/unpark_icon.gif");
		button = new JButton(discharge_icon);
		button.setText("Discharged Vehicle List");
		button.setSize(50, 30);
		button.setActionCommand("dischargeVehiclesView");
		button.addActionListener(this);
		add(button);

		ImageIcon help_icon = new ImageIcon("resources/images/help_icon.gif");
		button = new JButton(help_icon);
		button.setText("Help Desk");
		button.setSize(50, 30);
		button.setActionCommand("helpView");
		button.addActionListener(this);
		add(button);

		ImageIcon louout_icon = new ImageIcon("resources/images/help_icon.gif");
		button = new JButton(louout_icon);
		button.setText("Logout");
		button.setSize(50, 30);
		button.setActionCommand("logoutAction");
		button.addActionListener(this);
		add(button);

		ImageIcon exit_icon = new ImageIcon("resources/images/exit_icon.gif");
		button = new JButton(exit_icon);
		button.setSize(50, 30);
		button.setActionCommand("exit");
		button.addActionListener(this);

		add(button);
		navigationTf = new JTextField(15);
		// add(navigationTf);
		String activeScreen = "showParkedVehiclesView";
		loadScreens(activeScreen);
	}

	public void removeComponent(Component component) {
		if (component != null) {
			component.setVisible(false);
			remove(component);
		}
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		navigationTf.setText(event.getActionCommand());
		loadScreens(command);
	}

	public void loadScreens(String command) {
		removeComponent(footerPanel);
		removeComponent(loginPanel);
		removeComponent(addCustomerPanel);
		removeComponent(allCustomersPanel);
		removeComponent(parkVehicleNowPanel);
		removeComponent(parkedVehiclesPanel);
		removeComponent(unParkVehicleNowPanel);
		removeComponent(dischargedVehiclesPanel);
		removeComponent(helpPanel);

		JLabel panelDetails = null;
		// initialize visibilities
		if (ApplicationMain.isLogin) {
			switch (command) {
			case "addCustomerView":
				/////////////////////////////////////////////////////////////////////
				addCustomerPanel = new JPanel();
				// addCustomerPanel.setLayout(new GridBagLayout());
				addCustomerPanel.setMinimumSize(new Dimension(800, 550));
				addCustomerPanel.setMaximumSize(new Dimension(800, 550));
				addCustomerPanel.setPreferredSize(new Dimension(800, 550));

				ImageIcon add_customer = new ImageIcon("resources/images/add_customer.gif");

				addCustomerPanel.add(new JLabel(add_customer), BorderLayout.EAST);
				panelDetails = new JLabel("Add New Customer");
				panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
				addCustomerPanel.add(panelDetails, BorderLayout.EAST);

				addCustomerPanel.add(new AddCustomerForm());

				addCustomerPanel.setBackground(Color.yellow);
				add(addCustomerPanel);
				addFooter();
				break;
			case "showAllCustomersView":
				/////////////////////////////////////////////////////////////////////
				allCustomersPanel = new JPanel();
				allCustomersPanel.setMinimumSize(new Dimension(800, 550));
				allCustomersPanel.setMaximumSize(new Dimension(800, 550));
				allCustomersPanel.setPreferredSize(new Dimension(800, 550));

				ImageIcon user = new ImageIcon("resources/images/customers_icon.gif");

				allCustomersPanel.add(new JLabel(user), BorderLayout.EAST);
				panelDetails = new JLabel("List of All Customers");
				panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 30));

				allCustomersPanel.add(panelDetails, BorderLayout.EAST);
				allCustomersPanel.add(new CustomersListForm());

				allCustomersPanel.setBackground(Color.green);

				add(allCustomersPanel);
				addFooter();
				break;
			case "showParkedVehiclesView":
				/////////////////////////////////////////////////////////////////////
				parkedVehiclesPanel = new JPanel();
				parkedVehiclesPanel.setMinimumSize(new Dimension(800, 550));
				parkedVehiclesPanel.setMaximumSize(new Dimension(800, 550));
				parkedVehiclesPanel.setPreferredSize(new Dimension(800, 550));

				ImageIcon vehicle = new ImageIcon("resources/images/vehicle_icon.gif");

				parkedVehiclesPanel.add(new JLabel(vehicle), BorderLayout.LINE_START);
				panelDetails = new JLabel("Todays Parked Vehicles");
				panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
				parkedVehiclesPanel.add(panelDetails, BorderLayout.EAST);
				parkedVehiclesPanel.add(new ParkedVehiclesListForm());
				parkedVehiclesPanel.setBackground(Color.cyan);

				add(parkedVehiclesPanel);
				addFooter();
				break;
			case "parkVehicleNowView":

				/////////////////////////////////////////////////////////////////////
				parkVehicleNowPanel = new JPanel();
				parkVehicleNowPanel.setMinimumSize(new Dimension(800, 550));
				parkVehicleNowPanel.setMaximumSize(new Dimension(800, 550));
				parkVehicleNowPanel.setPreferredSize(new Dimension(800, 550));

				ImageIcon park_icon = new ImageIcon("resources/images/park_icon.gif");

				parkVehicleNowPanel.add(new JLabel(park_icon), BorderLayout.LINE_START);
				panelDetails = new JLabel("Vehicle Parking Add Form");
				panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
				parkVehicleNowPanel.add(panelDetails, BorderLayout.EAST);
				parkVehicleNowPanel.add(new ParkVehicleAddForm());
				parkVehicleNowPanel.setBackground(Color.magenta);

				add(parkVehicleNowPanel);
				addFooter();
				break;
			case "unParkVehicleNowView":
				/////////////////////////////////////////////////////////////////////
				unParkVehicleNowPanel = new JPanel();
				unParkVehicleNowPanel.setMinimumSize(new Dimension(800, 550));
				unParkVehicleNowPanel.setMaximumSize(new Dimension(800, 550));
				unParkVehicleNowPanel.setPreferredSize(new Dimension(800, 550));

				ImageIcon un_park_icon = new ImageIcon("resources/images/unpark_icon.gif");

				unParkVehicleNowPanel.add(new JLabel(un_park_icon), BorderLayout.LINE_START);
				panelDetails = new JLabel("Discharge Vehicle Form");
				panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
				unParkVehicleNowPanel.add(panelDetails, BorderLayout.EAST);
				unParkVehicleNowPanel.add(new UnParkVehicleForm());
				unParkVehicleNowPanel.setBackground(Color.magenta);

				add(unParkVehicleNowPanel);
				addFooter();
				break;
			case "dischargeVehiclesView":
				/////////////////////////////////////////////////////////////////////
				dischargedVehiclesPanel = new JPanel();
				dischargedVehiclesPanel.setMinimumSize(new Dimension(800, 550));
				dischargedVehiclesPanel.setMaximumSize(new Dimension(800, 550));
				dischargedVehiclesPanel.setPreferredSize(new Dimension(800, 550));

				ImageIcon discharge_icon = new ImageIcon("resources/images/unpark_icon.gif");

				dischargedVehiclesPanel.add(new JLabel(discharge_icon), BorderLayout.LINE_START);
				panelDetails = new JLabel("Discharged Vehicle List");
				panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
				dischargedVehiclesPanel.add(panelDetails, BorderLayout.EAST);
				dischargedVehiclesPanel.add(new UnParkedVehiclesListForm());
				dischargedVehiclesPanel.setBackground(Color.magenta);

				add(dischargedVehiclesPanel);
				addFooter();
				break;
			case "helpView":
				/////////////////////////////////////////////////////////////////////
				helpPanel = new JPanel();
				helpPanel.setMinimumSize(new Dimension(800, 550));
				helpPanel.setMaximumSize(new Dimension(800, 550));
				helpPanel.setPreferredSize(new Dimension(800, 550));

				ImageIcon help_icon = new ImageIcon("resources/images/help_icon.gif");

				helpPanel.add(new JLabel(help_icon), BorderLayout.LINE_START);
				panelDetails = new JLabel("Vehicle Management Help Desk");
				panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 30));
				helpPanel.add(panelDetails, BorderLayout.EAST);
				helpPanel.add(new HelpDetailsForm());
				helpPanel.setBackground(Color.magenta);

				add(helpPanel);
				addFooter();
				break;
			case "logoutAction":
				ApplicationMain.isLogin = false;
				loadScreens("showParkedVehiclesView");
				break;
			case "exit":
				System.exit(0);
				break;
			default:
				break;
			}

		} else {

			switch (command) {
			case "loginAction":
				if (userNameTf.getText().equals("admin") && passwordTf.getText().equals("1234")) {
					userNameTf.setText("");
					passwordTf.setText("");
					ApplicationMain.isLogin = true;
					String activeScreen = "showParkedVehiclesView";
					removeComponent(footerPanel);
					loadScreens(activeScreen);
					break;
				} else {
					/////////////////////////////////////////////////////////////////////
					loginPanel = new JPanel();
					loginPanel.setMinimumSize(new Dimension(800, 550));
					loginPanel.setMaximumSize(new Dimension(800, 550));
					loginPanel.setPreferredSize(new Dimension(800, 550));

					ImageIcon info_icon = new ImageIcon("resources/images/info_icon.gif");

					JLabel help_icon_label = new JLabel(info_icon);
					loginPanel.add(help_icon_label, BorderLayout.LINE_START);
					panelDetails = new JLabel(">>>>>>>>>>>>>>>> Please Login Now <<<<<<<<<<<<<");
					panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 35));
					loginPanel.add(panelDetails, BorderLayout.EAST);

					loginPanel.add(new JLabel("Username: "), BorderLayout.CENTER);
					userNameTf = new JTextField(15);
					loginPanel.add(userNameTf, BorderLayout.CENTER);
					loginPanel.add(new JLabel("Password: "), BorderLayout.CENTER);
					passwordTf = new JPasswordField(15);
					loginPanel.add(passwordTf, BorderLayout.CENTER);
					JButton loginButton = new JButton("Login");
					loginButton.setActionCommand("loginAction");
					loginButton.addActionListener(this);
					loginPanel.add(loginButton, BorderLayout.CENTER);

					loginPanel.setBackground(Color.LIGHT_GRAY);
					add(loginPanel);
					addFooter();
					break;
				}

			case "exit":
				System.exit(0);
				break;
			default:
				/////////////////////////////////////////////////////////////////////
				loginPanel = new JPanel();
				loginPanel.setMinimumSize(new Dimension(800, 550));
				loginPanel.setMaximumSize(new Dimension(800, 550));
				loginPanel.setPreferredSize(new Dimension(800, 550));

				ImageIcon info_icon = new ImageIcon("resources/images/info_icon.gif");

				JLabel help_icon_label = new JLabel(info_icon);
				loginPanel.add(help_icon_label, BorderLayout.LINE_START);
				panelDetails = new JLabel(">>>>>>>>>>>>>>>> Please Login Now <<<<<<<<<<<<<");
				panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 35));
				loginPanel.add(panelDetails, BorderLayout.EAST);

				loginPanel.add(new JLabel("Username: "), BorderLayout.CENTER);
				userNameTf = new JTextField(15);
				loginPanel.add(userNameTf, BorderLayout.CENTER);
				loginPanel.add(new JLabel("Password: "), BorderLayout.CENTER);
				passwordTf = new JPasswordField(15);
				loginPanel.add(passwordTf, BorderLayout.CENTER);
				JButton loginButton = new JButton("Login");
				loginButton.setActionCommand("loginAction");
				loginButton.addActionListener(this);
				loginPanel.add(loginButton, BorderLayout.CENTER);

				loginPanel.setBackground(Color.LIGHT_GRAY);

				add(loginPanel);
				addFooter();
				break;
			}
		}

	}

	public void addFooter() {
		/////////////////////////////////////////////////////////////////////
		footerPanel = new JPanel();
		footerPanel.setMinimumSize(new Dimension(900, 80));
		footerPanel.setMaximumSize(new Dimension(900, 80));
		footerPanel.setPreferredSize(new Dimension(900, 80));

		ImageIcon info_icon = new ImageIcon("resources/images/info_icon.gif");

		JLabel help_icon_label = new JLabel(info_icon);
		footerPanel.add(help_icon_label, BorderLayout.LINE_START);
		JLabel panelDetails = new JLabel("Powered By Md. Arifur Rahman, Newgen Technology Limited, Bangladesh");
		panelDetails.setFont(new Font("Comic Sans MS", Font.BOLD, 25));
		footerPanel.add(panelDetails, BorderLayout.EAST);
		footerPanel.setBackground(Color.LIGHT_GRAY);

		add(footerPanel);
	}

}
