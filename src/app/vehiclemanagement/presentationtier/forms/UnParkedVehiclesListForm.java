package app.vehiclemanagement.presentationtier.forms;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import app.vehiclemanagement.businesstier.ApplicationUtils;
import app.vehiclemanagement.businesstier.CustomerTO;
import app.vehiclemanagement.businesstier.VehicleRecordInfoTO;
import app.vehiclemanagement.persistencetier.CustomerService;
import app.vehiclemanagement.persistencetier.VehicleRecordInfoService;

public class UnParkedVehiclesListForm extends JPanel implements ActionListener {

	public UnParkedVehiclesListForm() {
		setBackground(Color.yellow);
		// petStrings = new String[];
		List<VehicleRecordInfoTO> recordInfos = VehicleRecordInfoService.getVehicleRecordInfos();

		setLayout(new GridBagLayout());
		String[] tableHeadline = { "Customer Name", "Phone No", "Vehicle Details", "Receive Time", "Payed ?" };
		DefaultTableModel model = new DefaultTableModel(tableHeadline, 0);
		if (recordInfos.size() >= 1) {
			for (VehicleRecordInfoTO vehicleRecordInfoTO : recordInfos) {
				Integer customerId = vehicleRecordInfoTO.getCustomerId();
				CustomerTO customerTO = CustomerService.getCustomerBy("customerId", customerId);

				if (customerTO != null && vehicleRecordInfoTO.getReceiveTime() != null) {
					String isPayed = vehicleRecordInfoTO.getIsPayed() == true ? "No" : "Yes";
					String hourMinuteStr = ApplicationUtils.getHourMinuteStr(vehicleRecordInfoTO.getReceiveTime());
					model.addRow(new String[] { customerTO.getName(), customerTO.getPhoneNo(),
							customerTO.getVehicleDescription(), hourMinuteStr, isPayed });

				}
			}
		}

		JTable table = new JTable(model);

		JLabel isbnLabel = new JLabel("ISBN: ");
		JTextField isbnTextFld = new JTextField(10);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;

		gbc.gridx++;
		gbc.gridy = 0;
		gbc.gridheight = gbc.REMAINDER;
		add(new JScrollPane(table), gbc);
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		switch (command) {
		case "action1":

			break;
		case "action2":

			break;
		case "exit":
			System.exit(0);
			break;
		default:
			break;
		}
	}

	public static void main(String[] args) {
		new UnParkedVehiclesListForm();
	}
}
