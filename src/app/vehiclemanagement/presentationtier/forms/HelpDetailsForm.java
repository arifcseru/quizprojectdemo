package app.vehiclemanagement.presentationtier.forms;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

public class HelpDetailsForm extends JPanel implements ActionListener {

	public HelpDetailsForm() {
		setBackground(Color.yellow);

		setLayout(new GridBagLayout());
		String[] columnNames = { "Questions", "Answers(Help Information Details)" };
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		model.addRow(new String[] { "How to Add Customer?", "click 'Add Customer' Button." });
		model.addRow(new String[] { "Can I see Customers List ?", "click 'All Customer' tab." });
		model.addRow(new String[] { "I want to see parked vehicle list.", "click 'Parked vehicle list' Button." });
		model.addRow(new String[] { "I want to park vehicle.", " click 'park vehicle' Button." });
		model.addRow(new String[] { "I want to discharge vehicle.", " click 'Discharge vehicle' Button." });
		model.addRow(new String[] { "How I see unparked vehicles list.", "click unparked vehicles Button." });
		model.addRow(new String[] { "Login or logout working ?", "Yes Click Logout Button" });

		JTable table = new JTable(model);

		JLabel isbnLabel = new JLabel("ISBN: ");
		JTextField isbnTextFld = new JTextField(10);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;

		gbc.gridx++;
		gbc.gridy = 0;
		gbc.gridheight = gbc.REMAINDER;
		add(new JScrollPane(table), gbc);
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();

		switch (command) {
		case "addCustomerView":
			break;
		case "exit":
			System.exit(0);
			break;
		default:
			break;
		}
	}
}