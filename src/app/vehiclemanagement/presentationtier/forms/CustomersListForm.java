package app.vehiclemanagement.presentationtier.forms;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import app.vehiclemanagement.businesstier.CustomerTO;
import app.vehiclemanagement.businesstier.VehicleRecordInfoTO;
import app.vehiclemanagement.persistencetier.CustomerService;
import app.vehiclemanagement.persistencetier.VehicleRecordInfoService;

public class CustomersListForm extends JPanel implements ActionListener {

	public CustomersListForm() {
		setBackground(Color.yellow);
		// petStrings = new String[];
		List<CustomerTO> customers = CustomerService.getCustomers();

		setLayout(new GridBagLayout());
		String[] columnNames = { "Name", "Phone No", "Vehicle Details", "Parked or Discharged ?" };
		DefaultTableModel model = new DefaultTableModel(columnNames, 0);
		if (customers.size() >= 1) {
			for (CustomerTO customerTO : customers) {
				Integer customerId = customerTO.getCustomerId();

				VehicleRecordInfoTO recordInfoTO = VehicleRecordInfoService.getVehicleRecordInfoBy("customerId",
						customerId);
				String dischargeOrParkedOrNone = "None";
				if (recordInfoTO != null) {
					dischargeOrParkedOrNone = recordInfoTO.getReceiveTime() == null ? "Parked" : "Discharged";
				}
				model.addRow(new String[] { customerTO.getName(), customerTO.getPhoneNo(),
						customerTO.getVehicleDescription(), dischargeOrParkedOrNone });
			}
		}

		JTable table = new JTable(model);

		JLabel isbnLabel = new JLabel("ISBN: ");
		JTextField isbnTextFld = new JTextField(10);

		GridBagConstraints gbc = new GridBagConstraints();
		gbc.gridx = 0;
		gbc.gridy = 0;

		gbc.gridx++;
		gbc.gridy = 0;
		gbc.gridheight = gbc.REMAINDER;
		add(new JScrollPane(table), gbc);
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();

		switch (command) {
		case "addCustomerView":
			break;
		case "exit":
			System.exit(0);
			break;
		default:
			break;
		}
	}
}
