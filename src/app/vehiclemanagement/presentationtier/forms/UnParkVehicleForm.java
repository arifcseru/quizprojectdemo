package app.vehiclemanagement.presentationtier.forms;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import app.vehiclemanagement.businesstier.ApplicationUtils;
import app.vehiclemanagement.businesstier.CustomerTO;
import app.vehiclemanagement.businesstier.VehicleRecordInfoTO;
import app.vehiclemanagement.persistencetier.CustomerService;
import app.vehiclemanagement.persistencetier.VehicleRecordInfoService;

public class UnParkVehicleForm extends JPanel implements ActionListener {
	JTextField customerIdTf = null;
	JTextField phoneNoTf = null;
	JTextField vehicleDescriptionTf = null;

	JTextField recordInfoIdTf = null;
	JTextField inTimeTf = null;
	JTextField outTimeTf = null;
	JTextField receiveTimeTf = null;
	JCheckBox isBlockedCheck = null;
	JCheckBox isPayedCheck = null;
	JTextField palceRentTf = null;
	JTextArea remarksTa = null;
	JLabel successLabel = null;
	JComboBox customersDropdown = null;

	public UnParkVehicleForm() {
		setBackground(Color.yellow);

		setLayout(new GridBagLayout());

		JLabel customerNameLabel = new JLabel("Customer Name: ");
		List<CustomerTO> customers = CustomerService.getCustomers();
		List<VehicleRecordInfoTO> parkedVehicleCustomers = new ArrayList<>();

		int i = 0;
		for (CustomerTO customerTO : customers) {
			Integer customerId = customerTO.getCustomerId();
			VehicleRecordInfoTO recordInfoTO = VehicleRecordInfoService.getVehicleRecordInfoBy("customerId",
					customerId);
			if (recordInfoTO != null) {
				parkedVehicleCustomers.add(recordInfoTO);
			}
		}
		String[] customerNameList = new String[parkedVehicleCustomers.size() + 1];
		for (CustomerTO customerTO : customers) {
			Integer customerId = customerTO.getCustomerId();
			VehicleRecordInfoTO recordInfoTO = VehicleRecordInfoService.getVehicleRecordInfoBy("customerId",
					customerId);
			// parkedVehicleCustomers.add(recordInfoTO);
			if (recordInfoTO != null && recordInfoTO.getReceiveTime() == null) {
				customerNameList[i++] = customerTO.getName();
			}
		}
		customersDropdown = new JComboBox(customerNameList);
		customersDropdown.setSelectedIndex(1);
		customersDropdown.setActionCommand("showCustomerDetails");
		customersDropdown.addActionListener(this);

		JLabel customerIdLabel = new JLabel("Customer Id: ");
		customerIdTf = new JTextField(10);

		JLabel phoneNoLabel = new JLabel("Phone Number: ");
		phoneNoTf = new JTextField(10);
		phoneNoTf.setDisabledTextColor(Color.BLACK);
		phoneNoTf.setEnabled(false);

		JLabel vehicleDescriptionLabel = new JLabel("Vehicle Description: ");
		vehicleDescriptionTf = new JTextField(10);
		vehicleDescriptionTf.setDisabledTextColor(Color.BLACK);
		vehicleDescriptionTf.setEnabled(false);

		JLabel recordInfoIdLabel = new JLabel("Record Info Id: ");
		recordInfoIdTf = new JTextField(10);
		recordInfoIdTf.setDisabledTextColor(Color.BLACK);
		recordInfoIdTf.setEnabled(false);

		JLabel inTimeLabel = new JLabel("In Time: ");
		inTimeTf = new JTextField(10);
		inTimeTf.setDisabledTextColor(Color.BLACK);
		inTimeTf.setEnabled(false);

		JLabel outTimeLabel = new JLabel("Out Time: ");
		outTimeTf = new JTextField(10);
		outTimeTf.setDisabledTextColor(Color.BLACK);
		outTimeTf.setEnabled(false);

		JLabel receiveTimeLabel = new JLabel("Receive Time: ");
		receiveTimeTf = new JTextField(10);
		receiveTimeTf.setText("04:30");

		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;

		isBlockedCheck = new JCheckBox("Blocked ?");
		isPayedCheck = new JCheckBox("Payed ?");

		add(customerNameLabel, gridBagConstraints);

		gridBagConstraints.gridy++;
		add(customerIdLabel, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(phoneNoLabel, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(vehicleDescriptionLabel, gridBagConstraints);

		gridBagConstraints.gridy++;
		add(recordInfoIdLabel, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(inTimeLabel, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(outTimeLabel, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(receiveTimeLabel, gridBagConstraints);

		gridBagConstraints.gridx++;

		gridBagConstraints.gridy = 0;
		add(customersDropdown, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(customerIdTf, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(phoneNoTf, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(vehicleDescriptionTf, gridBagConstraints);

		gridBagConstraints.gridy++;
		add(recordInfoIdTf, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(inTimeTf, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(outTimeTf, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(receiveTimeTf, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(isBlockedCheck, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(isPayedCheck, gridBagConstraints);

		JButton saveButton = new JButton("Save");
		saveButton.setSize(50, 30);
		saveButton.setActionCommand("unParkVehicleAction");
		saveButton.addActionListener(this);
		gridBagConstraints.gridy++;
		add(saveButton, gridBagConstraints);

		successLabel = new JLabel("");
		gridBagConstraints.gridy++;
		add(successLabel, gridBagConstraints);

		gridBagConstraints.gridx++;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridheight = gridBagConstraints.REMAINDER;
		remarksTa = new JTextArea(10, 30);
		JLabel blankLabel = new JLabel("Testing");
		blankLabel.setSize(200, 50);
		remarksTa.setBackground(Color.magenta);
		add(new JScrollPane(remarksTa), gridBagConstraints);
	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();
		CustomerTO customerTO = null;
		String customerName = null;
		switch (command) {
		case "unParkVehicleAction":
			VehicleRecordInfoTO vehicleRecordInfoTO = new VehicleRecordInfoTO();
			customerName = (String) customersDropdown.getSelectedItem();
			customerTO = CustomerService.getCustomerBy("name", customerName);
			if (customerTO != null) {
				Integer customerId = customerTO.getCustomerId();
				vehicleRecordInfoTO.setCustomerId(customerId);
			}
			String inTimeStr = receiveTimeTf.getText();
			if (inTimeStr.equals(null) || inTimeStr != "") {
				Timestamp inTime = ApplicationUtils.getTodayTimestamp(inTimeStr);
				vehicleRecordInfoTO.setInTime(inTime);
			}
			String outTimeStr = receiveTimeTf.getText();
			if (outTimeStr.equals(null) || outTimeStr != "") {
				Timestamp outTime = ApplicationUtils.getTodayTimestamp(outTimeStr);
				vehicleRecordInfoTO.setOutTime(outTime);
			}
			String receiveTimeStr = receiveTimeTf.getText();
			if (receiveTimeStr.equals(null) || receiveTimeStr != "") {
				Timestamp receiveTime = ApplicationUtils.getTodayTimestamp(receiveTimeStr);
				vehicleRecordInfoTO.setReceiveTime(receiveTime);
			}
			Boolean isPayed = isPayedCheck.isSelected();
			vehicleRecordInfoTO.setIsPayed(isPayed);
			Boolean isBlocked = isBlockedCheck.isSelected();
			vehicleRecordInfoTO.setIsBlocked(isBlocked);
			Integer recordInfoId = Integer.parseInt(recordInfoIdTf.getText());
			if (isPayed && recordInfoId != null) {
				Boolean isSuccess = VehicleRecordInfoService.updateVehicleRecordInfo(recordInfoId, vehicleRecordInfoTO);
				if (isSuccess) {
					successLabel.setText("Vehicle Successfully discharged! ");
				} else {
					successLabel.setText("Error to discharge! ");
				}
			} else {
				successLabel.setText("Please Pay first! ");
			}

			break;
		case "showCustomerDetails":
			customerName = (String) customersDropdown.getSelectedItem();
			customerTO = CustomerService.getCustomerBy("name", customerName);
			if (customerTO != null) {
				Integer customerId = customerTO.getCustomerId();
				customerIdTf.setText(customerId.toString());
				phoneNoTf.setText(customerTO.getPhoneNo());
				vehicleDescriptionTf.setText(customerTO.getVehicleDescription());
				VehicleRecordInfoTO vehicleRecordInfo = VehicleRecordInfoService.getVehicleRecordInfoBy("customerId",
						customerId);
				if (vehicleRecordInfo != null) {
					recordInfoIdTf.setText(vehicleRecordInfo.getRecordInfoId().toString());
					if (vehicleRecordInfo.getInTime() != null) {
						String hourMinuteStr = ApplicationUtils.getHourMinuteStr(vehicleRecordInfo.getInTime());
						inTimeTf.setText(hourMinuteStr);
					}
					if (vehicleRecordInfo.getOutTime() != null) {
						String hourMinuteStr = ApplicationUtils.getHourMinuteStr(vehicleRecordInfo.getOutTime());
						outTimeTf.setText(hourMinuteStr);
					}

				}
			}
			break;
		case "exit":
			System.exit(0);
			break;
		default:
			break;
		}
	}

	public static void main(String[] args) {
		new UnParkVehicleForm();
	}
}
