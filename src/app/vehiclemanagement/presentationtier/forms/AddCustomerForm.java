package app.vehiclemanagement.presentationtier.forms;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import app.vehiclemanagement.businesstier.CustomerTO;
import app.vehiclemanagement.persistencetier.CustomerService;

public class AddCustomerForm extends JPanel implements ActionListener {
	JTextField nameTf = null;
	JTextField phoneNoTf = null;
	JTextArea vehicleDescriptionTa = null;
	JLabel successLabel = null;

	public AddCustomerForm() {
		setBackground(Color.yellow);
		
		setLayout(new GridBagLayout());

		JLabel nameLabel = new JLabel("Name: ");
		nameTf = new JTextField(10);

		JLabel phoneNoLabel = new JLabel("Phone No: ");
		phoneNoTf = new JTextField(10);

		JLabel vehicleDescriptionLabel = new JLabel("Vehicle Description: ");
		vehicleDescriptionTa = new JTextArea(10, 30);

		GridBagConstraints gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;

		add(nameLabel, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(phoneNoLabel, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(vehicleDescriptionLabel, gridBagConstraints);

		gridBagConstraints.gridx++;
		gridBagConstraints.gridy = 0;
		add(nameTf, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(phoneNoTf, gridBagConstraints);
		gridBagConstraints.gridy++;
		add(vehicleDescriptionTa, gridBagConstraints);

		JButton saveButton = new JButton("Save");
		saveButton.setSize(50, 30);
		saveButton.setActionCommand("addCustomerAction");
		saveButton.addActionListener(this);
		gridBagConstraints.gridy++;
		add(saveButton, gridBagConstraints);

		successLabel = new JLabel("");
		gridBagConstraints.gridy++;
		add(successLabel, gridBagConstraints);

		gridBagConstraints.gridx++;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.gridheight = gridBagConstraints.REMAINDER;
		add(new JScrollPane(vehicleDescriptionTa), gridBagConstraints);

	}

	public void actionPerformed(ActionEvent event) {
		String command = event.getActionCommand();

		switch (command) {
		case "addCustomerAction":
			CustomerTO customerTO = new CustomerTO();
			customerTO.setName(nameTf.getText());
			customerTO.setPhoneNo(phoneNoTf.getText());
			customerTO.setVehicleDescription(vehicleDescriptionTa.getText());
			Integer customerId = CustomerService.createCustomer(customerTO);
			successLabel.setText("Customer Created with customer id:" + customerId);
			break;
		case "exit":
			System.exit(0);
			break;
		default:
			break;
		}
	}
}
