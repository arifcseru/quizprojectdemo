package app.vehiclemanagement.persistencetier;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import app.vehiclemanagement.businesstier.VehicleRecordInfoTO;

public class VehicleRecordInfoService {
	public static String databaseName = "database/VehicleGarrageManagement.db";

	public static Integer createVehicleRecordInfo(VehicleRecordInfoTO vehicleRecordInfoTO) {
		Integer questionSetId = null;
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "INSERT INTO VehicleRecordInfo (customerId,inTime,outTime,receiveTime,isBlocked,isPayed,placeRent,remarks) VALUES('"
					+ vehicleRecordInfoTO.getCustomerId() + "','" + vehicleRecordInfoTO.getInTime() + "','"
					+ vehicleRecordInfoTO.getOutTime() + "','" + vehicleRecordInfoTO.getReceiveTime() + "','"
					+ vehicleRecordInfoTO.getIsBlocked() + "','" + vehicleRecordInfoTO.getIsPayed() + "','"
					+ vehicleRecordInfoTO.getPlaceRent() + "','" + vehicleRecordInfoTO.getRemarks() + "')";
			statement.executeUpdate(sql);
			statement.close();
			statement = connection.createStatement();
			questionSetId = statement.getGeneratedKeys().getInt(1);
			// sql = "INSERT INTO roundQuestions (recordInfoId,roundId)
			// VALUES('"+recordInfoId+"','"+roundId+"')";
			// statement.executeUpdate(sql);
			// System.out.println(recordInfoId);
			// System.out.println();
			// statement.close();

		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex);
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex);
		}
		// System.out.println("dksjaf");
		return questionSetId;
	}

	public static List<VehicleRecordInfoTO> getVehicleRecordInfos() {
		Connection connection = null;
		Statement statement = null;
		VehicleRecordInfoTO vehicleRecordInfoTO = null;
		List<VehicleRecordInfoTO> recordInfos = new ArrayList<VehicleRecordInfoTO>();
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "SELECT * FROM VehicleRecordInfo ORDER BY customerId ASC";
			// c.prepareStatement(sql, roundId);
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				vehicleRecordInfoTO = new VehicleRecordInfoTO();
				vehicleRecordInfoTO.setRecordInfoId(Integer.parseInt(rs.getString("recordInfoId")));
				vehicleRecordInfoTO.setCustomerId(rs.getInt("customerId"));

				if (!rs.getObject("inTime").equals("null")) {
					vehicleRecordInfoTO.setInTime(rs.getTimestamp("inTime"));
				}
				if (!rs.getObject("outTime").equals("null")) {
					vehicleRecordInfoTO.setOutTime(rs.getTimestamp("outTime"));
				}
				if (!rs.getObject("receiveTime").equals("null")) {
					vehicleRecordInfoTO.setReceiveTime(rs.getTimestamp("receiveTime"));
				}
				vehicleRecordInfoTO.setIsBlocked(rs.getBoolean("isBlocked"));
				vehicleRecordInfoTO.setIsPayed(rs.getBoolean("isPayed"));
				vehicleRecordInfoTO.setPlaceRent(rs.getFloat("placeRent"));
				vehicleRecordInfoTO.setRemarks(rs.getString("remarks"));
				recordInfos.add(vehicleRecordInfoTO);
			}
			statement.close();
			connection.close();

		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(null, ex.getMessage());
		} catch (SQLException ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		return recordInfos;
	}

	public static VehicleRecordInfoTO getVehicleRecordInfoBy(String fieldName, Object fieldValue) {
		Connection connection = null;
		Statement statement = null;
		VehicleRecordInfoTO vehicleRecordInfoTO = null;
		List<VehicleRecordInfoTO> recordInfos = new ArrayList<VehicleRecordInfoTO>();
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "SELECT * FROM VehicleRecordInfo WHERE " + fieldName + "='" + fieldValue
					+ "' ORDER BY customerId ASC";
			// c.prepareStatement(sql, roundId);
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				vehicleRecordInfoTO = new VehicleRecordInfoTO();
				vehicleRecordInfoTO.setRecordInfoId(Integer.parseInt(rs.getString("recordInfoId")));
				vehicleRecordInfoTO.setCustomerId(rs.getInt("customerId"));
				if (!rs.getObject("inTime").equals("null")) {
					vehicleRecordInfoTO.setInTime(rs.getTimestamp("inTime"));
				}
				if (!rs.getObject("outTime").equals("null")) {
					vehicleRecordInfoTO.setOutTime(rs.getTimestamp("outTime"));
				}
				if (!rs.getObject("receiveTime").equals("null")) {
					vehicleRecordInfoTO.setReceiveTime(rs.getTimestamp("receiveTime"));
				}
				vehicleRecordInfoTO.setIsBlocked(rs.getBoolean("isBlocked"));
				vehicleRecordInfoTO.setIsPayed(rs.getBoolean("isPayed"));
				vehicleRecordInfoTO.setPlaceRent(rs.getFloat("placeRent"));
				vehicleRecordInfoTO.setRemarks(rs.getString("remarks"));
				recordInfos.add(vehicleRecordInfoTO);
			}
			statement.close();
			connection.close();

		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		if (recordInfos.size() >= 1) {
			return recordInfos.get(0);
		} else {
			return null;
		}

	}

	public static Boolean updateVehicleRecordInfo(Integer recordInfoId, VehicleRecordInfoTO vehicleRecordInfoTO) {
		Boolean updateResult = false;
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "UPDATE vehicleRecordInfo SET customerId='" + vehicleRecordInfoTO.getCustomerId() + "', inTime='"
					+ vehicleRecordInfoTO.getInTime() + "', outTime='" + vehicleRecordInfoTO.getOutTime()
					+ "', receiveTime='" + vehicleRecordInfoTO.getReceiveTime() + "', isBlocked='"
					+ vehicleRecordInfoTO.getIsBlocked() + "', isPayed='" + vehicleRecordInfoTO.getIsPayed()
					+ "', placeRent='" + vehicleRecordInfoTO.getPlaceRent() + "', remarks='"
					+ vehicleRecordInfoTO.getRemarks() + "' WHERE recordInfoId = " + recordInfoId;
			// c.prepareStatement(sql, roundId);
			statement.executeUpdate(sql);
			statement.close();
			connection.close();
			updateResult = true;
		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		return updateResult;
	}

	public static Boolean deleteVehicleRecordInfo(Integer recordInfoId) {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "DELETE FROM vehicleRecordInfo WHERE recordInfoId = '" + recordInfoId + "'";
			statement.executeUpdate(sql);
			statement.close();
			connection.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return true;
	}

	public static void main(String[] args) {
		VehicleRecordInfoTO vehicleRecordInfoTO = new VehicleRecordInfoTO();
		vehicleRecordInfoTO.setCustomerId(1000);
		// vehicleRecordInfoTO.setInTime(new Timestamp());
		vehicleRecordInfoTO = getVehicleRecordInfoBy("recordInfoId", 3);
		Float placeRent = new Float(40);
		vehicleRecordInfoTO.setPlaceRent(placeRent);
		Boolean result = updateVehicleRecordInfo(3, vehicleRecordInfoTO);
		System.out.println(result);
	}
}
