package app.vehiclemanagement.persistencetier;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import app.vehiclemanagement.businesstier.CustomerTO;

public class CustomerService {
	public static String databaseName = "database/VehicleGarrageManagement.db";

	public static Integer createCustomer(CustomerTO customerTO) {
		Integer questionSetId = null;
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "INSERT INTO Customers (name,phoneNo,vehicleDescription) VALUES('" + customerTO.getName()
					+ "','" + customerTO.getPhoneNo() + "','" + customerTO.getVehicleDescription() + "')";
			statement.executeUpdate(sql);
			statement.close();
			statement = connection.createStatement();
			questionSetId = statement.getGeneratedKeys().getInt(1);
			// sql = "INSERT INTO roundQuestions (customerId,roundId)
			// VALUES('"+customerId+"','"+roundId+"')";
			// statement.executeUpdate(sql);
			// System.out.println(customerId);
			// System.out.println();
			// statement.close();

		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex);
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex);
		}
		// System.out.println("dksjaf");
		return questionSetId;
	}

	public static List<CustomerTO> getCustomers() {
		Connection connection = null;
		Statement statement = null;
		CustomerTO customerTO = null;
		List<CustomerTO> users = new ArrayList<CustomerTO>();
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "SELECT * FROM customers ORDER BY name ASC";
			// c.prepareStatement(sql, roundId);
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				customerTO = new CustomerTO();
				customerTO.setCustomerId(Integer.parseInt(rs.getString("customerId")));
				customerTO.setName(rs.getString("name"));
				customerTO.setPhoneNo(rs.getString("phoneNo"));
				customerTO.setVehicleDescription(rs.getString("vehicleDescription"));
				users.add(customerTO);
			}
			statement.close();
			connection.close();

		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		return users;
	}

	public static CustomerTO getCustomerBy(String fieldName, Object fieldValue) {
		Connection connection = null;
		Statement statement = null;
		CustomerTO customerTO = null;
		List<CustomerTO> customers = new ArrayList<CustomerTO>();
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "SELECT * FROM customers WHERE " + fieldName + "='" + fieldValue + "' ORDER BY name ASC";
			// c.prepareStatement(sql, roundId);
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				customerTO = new CustomerTO();
				customerTO.setCustomerId(Integer.parseInt(rs.getString("customerId")));
				customerTO.setName(rs.getString("name"));
				customerTO.setPhoneNo(rs.getString("phoneNo"));
				customerTO.setVehicleDescription(rs.getString("vehicleDescription"));
				customers.add(customerTO);
			}
			statement.close();
			connection.close();

		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		if (customers.size()>=1) {
			return customers.get(0);
		}
		else return null;
	}

	public static Boolean updateCustomer(Integer customerId, CustomerTO customerTO) {
		Boolean updateResult = false;
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "UPDATE customers SET " + "name=" + customerTO.getName() + ", phoneNo="
					+ customerTO.getPhoneNo() + ", vehicleDescription=" + customerTO.getVehicleDescription()
					+ " WHERE customerId = " + customerId;
			// c.prepareStatement(sql, roundId);
			statement.executeUpdate(sql);
			statement.close();
			connection.close();
			updateResult = true;
		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		return updateResult;
	}

	public static Boolean deleteCustomer(Integer customerId) {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "DELETE FROM customers WHERE customerId = '" + customerId + "'";
			statement.executeUpdate(sql);
			statement.close();
			connection.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return true;
	}

	public static void main(String[] args) {
		CustomerTO customerTO = new CustomerTO();
		/*customerTO.setName("Arif");
		customerTO.setPhoneNo("012233");
		customerTO.setVehicleDescription("Bi Cycle");
		System.out.println(createCustomer(customerTO));*/
		
		String fieldName = "phoneNo";
		String fieldValue = "012233";
		customerTO = getCustomerBy(fieldName, fieldValue);
		if (customerTO!=null) {
			System.out.println(customerTO.getName());			
		}else{
			System.out.println("Customer not found!");
		}
	}
}
