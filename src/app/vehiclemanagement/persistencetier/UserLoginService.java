package app.vehiclemanagement.persistencetier;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import app.vehiclemanagement.businesstier.UserLoginTO;

public class UserLoginService {
	public static String databaseName = "database/VehicleGarrageManagement.db";

	public static Integer createUserLogin(UserLoginTO userLoginTO) {
		Integer questionSetId = null;
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "INSERT INTO UserLogin (username,password,userCreationDate,userModificationDate) VALUES('"
					+ userLoginTO.getUserName() + "','" + userLoginTO.getPassword() + "','"
					+ userLoginTO.getUserCreationDate() + "','" + userLoginTO.getUserModificationDate() + "')";
			statement.executeUpdate(sql);
			statement.close();
			statement = connection.createStatement();
			questionSetId = statement.getGeneratedKeys().getInt(1);
			// sql = "INSERT INTO roundQuestions (UserLoginId,roundId)
			// VALUES('"+UserLoginId+"','"+roundId+"')";
			// statement.executeUpdate(sql);
			// System.out.println(UserLoginId);
			// System.out.println();
			// statement.close();

		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex);
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex);
		}
		// System.out.println("dksjaf");
		return questionSetId;
	}

	public static List<UserLoginTO> getUserLogins() {
		Connection connection = null;
		Statement statement = null;
		UserLoginTO userLoginTO = null;
		List<UserLoginTO> users = new ArrayList<UserLoginTO>();
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "SELECT * FROM userLogin ORDER BY userName ASC";
			// c.prepareStatement(sql, roundId);
			ResultSet rs = statement.executeQuery(sql);
			while (rs.next()) {
				userLoginTO = new UserLoginTO();
				userLoginTO.setUserLoginId(Integer.parseInt(rs.getString("UserLoginId")));
				userLoginTO.setUserName(rs.getString("userName"));
				userLoginTO.setPassword(rs.getString("password"));
				userLoginTO.setUserCreationDate(rs.getTimestamp("userCreationDate"));
				users.add(userLoginTO);
			}
			statement.close();
			connection.close();

		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		return users;
	}

	public static Boolean updateUserLogin(Integer UserLoginId, UserLoginTO userLoginTO) {
		Boolean updateResult = false;
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "UPDATE userLogin SET  userName=" + userLoginTO.getUserName() + ", password="
					+ userLoginTO.getPassword() + ", userCreationDate=" + userLoginTO.getUserCreationDate()
					+ " WHERE UserLoginId = " + UserLoginId;
			// c.prepareStatement(sql, roundId);
			statement.executeUpdate(sql);
			statement.close();
			connection.close();
			updateResult = true;
		} catch (ClassNotFoundException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		} catch (SQLException ex) {
			JOptionPane.showMessageDialog(null, ex.getMessage());
		}
		return updateResult;
	}

	public static Boolean deleteUserLogin(Integer userLoginId) {
		Connection connection = null;
		Statement statement = null;
		try {
			Class.forName("org.sqlite.JDBC");
			connection = DriverManager.getConnection("jdbc:sqlite:" + databaseName);
			statement = connection.createStatement();
			String sql = "DELETE FROM userLogin WHERE userLoginId = '" + userLoginId + "'";
			statement.executeUpdate(sql);
			statement.close();
			connection.close();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.getMessage());
		}
		return true;
	}

	public static void main(String[] args) {
		UserLoginTO userLoginTO = new UserLoginTO();
		userLoginTO.setUserName("Arif");
		userLoginTO.setPassword("012233");
		// userLoginTO.setuserCreationDate("Bi Cycle");
		System.out.println(createUserLogin(userLoginTO));
	}
}
