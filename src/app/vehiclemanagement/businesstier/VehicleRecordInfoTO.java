package app.vehiclemanagement.businesstier;

import java.sql.Timestamp;

public class VehicleRecordInfoTO {
	private Integer recordInfoId;
	private Integer customerId;
	private Timestamp inTime;
	private Timestamp outTime;
	private Timestamp receiveTime;
	private Boolean isBlocked;
	private Boolean isPayed;
	private Float placeRent;
	private String remarks;

	public Integer getRecordInfoId() {
		return recordInfoId;
	}

	public void setRecordInfoId(Integer recordInfoId) {
		this.recordInfoId = recordInfoId;
	}

	public Boolean getIsPayed() {
		return isPayed;
	}

	public void setIsPayed(Boolean isPayed) {
		this.isPayed = isPayed;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Timestamp getInTime() {
		return inTime;
	}

	public void setInTime(Timestamp inTime) {
		this.inTime = inTime;
	}

	public Timestamp getOutTime() {
		return outTime;
	}

	public void setOutTime(Timestamp outTime) {
		this.outTime = outTime;
	}

	public Timestamp getReceiveTime() {
		return receiveTime;
	}

	public void setReceiveTime(Timestamp receiveTime) {
		this.receiveTime = receiveTime;
	}

	public Boolean getIsBlocked() {
		return isBlocked;
	}

	public void setIsBlocked(Boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public Float getPlaceRent() {
		return placeRent;
	}

	public void setPlaceRent(Float placeRent) {
		this.placeRent = placeRent;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

}
