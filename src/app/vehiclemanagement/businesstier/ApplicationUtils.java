package app.vehiclemanagement.businesstier;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @project_name baby
 * @author arifur
 * @date Dec 7, 2015 @time 4:06:03 PM
 * @file_name PlanningUtils.java
 */
public class ApplicationUtils {

	public static String getRandomColor() {
		String[] letters = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F" };
		String color = "#";
		for (int i = 0; i < 6; i++) {
			color += letters[(int) Math.round(Math.random() * 15)];
		}
		return color;
	}

	/**
	 * @author arifur
	 * @created_date_time Dec 7, 2015 => 4:03:28 PM
	 * @param ts
	 * @param days
	 * @return
	 */
	public static Timestamp getDateAfterAdditionOfDays(Timestamp ts, Long days) {
		java.sql.Timestamp dateTSAfterAddition = new Timestamp(ts.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTSAfterAddition);
		cal.add(Calendar.DAY_OF_WEEK, days.intValue());
		dateTSAfterAddition.setTime(cal.getTime().getTime()); // or
		dateTSAfterAddition = new Timestamp(cal.getTime().getTime());
		return dateTSAfterAddition;
	}

	public static Timestamp getDateAfterSubtractionOfDays(Timestamp ts, Long days) {
		java.sql.Timestamp dateTSAfterSubtraction = new Timestamp(ts.getTime());
		Calendar cal = Calendar.getInstance();
		cal.setTime(dateTSAfterSubtraction);
		cal.add(Calendar.DAY_OF_WEEK, -days.intValue());
		dateTSAfterSubtraction.setTime(cal.getTime().getTime()); // or
		dateTSAfterSubtraction = new Timestamp(cal.getTime().getTime());
		return dateTSAfterSubtraction;
	}

	public static List<String> getFirstAndLastDate(int year, int month) {
		List<String> dateLists = new ArrayList();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		int date = 1;
		int maxDay = 0;
		calendar.set(year, month, date);
		String startDateStr = formatter.format(calendar.getTime());

		System.out.println("First Day: " + formatter.format(calendar.getTime()));

		// Getting Maximum day for Given Month
		maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(year, month, maxDay);
		String endDateStr = formatter.format(calendar.getTime());
		System.out.println("Last Day: " + formatter.format(calendar.getTime()));

		dateLists.add(0, startDateStr);
		dateLists.add(1, endDateStr);
		return dateLists;
	}

	public static List<String> getFirstAndLastDateOfYear(int year) {
		List<String> dateLists = new ArrayList();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Calendar calendar = Calendar.getInstance();
		int date = 1;
		int maxDay = 0;
		calendar.set(year, 0, date);
		String startDateStr = formatter.format(calendar.getTime());

		System.out.println("First Day: " + formatter.format(calendar.getTime()));

		// Getting Maximum day for Given Month
		maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(year, 11, maxDay);
		String endDateStr = formatter.format(calendar.getTime());
		System.out.println("Last Day: " + formatter.format(calendar.getTime()));

		dateLists.add(0, startDateStr);
		dateLists.add(1, endDateStr);
		return dateLists;
	}

	public static List<Timestamp> getFirstAndLastTimestamp(int year, int month) {

		List<Timestamp> dateLists = new ArrayList();
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat dformatter = new SimpleDateFormat("yyyy-MM-dd");

		calendar.set(year, month, 1);
		String startDateStr = dformatter.format(calendar.getTime());
		int maxDay = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(year, month, maxDay);
		String endDateStr = dformatter.format(calendar.getTime());

		java.sql.Timestamp startDate;
		startDate = getTimestampFromString(startDateStr, dformatter);
		java.sql.Timestamp endDate;
		endDate = getTimestampFromString(endDateStr, dformatter);

		dateLists.add(0, startDate);
		dateLists.add(1, endDate);
		return dateLists;

	}

	public static String getMonthName(int month, int year) {

		String monthName = null;
		SimpleDateFormat formatter = new SimpleDateFormat("MMMMM");

		Calendar calendar = Calendar.getInstance();
		calendar.set(year, month, 1);
		monthName = formatter.format(calendar.getTime());

		return monthName;

	}

	public static Long getYearLong(Timestamp date) {
		Long year = null;
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		year = (long) cal.get(Calendar.YEAR);
		return year;
	}

	public static Long getMonthInt(Timestamp date) {
		Long monthLong = null;
		SimpleDateFormat formatter = new SimpleDateFormat("MM");
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(date.getTime());
		monthLong = Long.parseLong(formatter.format(calendar.getTime())) - new Long(1);
		return monthLong;
	}

	public static Date convertDateOneFormatToAnother(String dateinString) {
		final String OLD_FORMAT = "dd/MM/yyyy";
		final String NEW_FORMAT = "yyyy-MM-dd";

		String newDateString;

		SimpleDateFormat sdf = new SimpleDateFormat(OLD_FORMAT);
		Date dateOld = null;
		try {
			dateOld = sdf.parse(dateinString);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		sdf.applyPattern(NEW_FORMAT);
		newDateString = sdf.format(dateOld);
		Date shipmentDate = java.sql.Date.valueOf(newDateString.toString());
		return shipmentDate;
	}

	/**
	 * @author arifur
	 * @created_date_time Dec 7, 2015 => 4:03:28 PM
	 * @param str_date
	 * @param formatter
	 * @return
	 */
	public static Timestamp getTimestampFromString(String str_date, DateFormat formatter) {
		try {

			// you can change format of date
			Date date = formatter.parse(str_date);
			java.sql.Timestamp timeStampDate = new Timestamp(date.getTime());

			return timeStampDate;
		} catch (ParseException e) {
			System.out.println("Exception :" + e);
			return null;
		}
	}

	public static String getStringFromTimestamp(Timestamp timeStampDate, DateFormat formatter) {
		// you can change format of date
		String str_date = "";
		Date date = new Date(timeStampDate.getTime());
		str_date = formatter.format(date);

		return str_date;
	}

	/**
	 * @author arifur
	 * @created_date_time Dec 7, 2015 => 4:03:28 PM
	 * @param month
	 * @param year
	 * @return
	 */
	public static Long getTotalDaysInMonth(int month, int year) {
		month = month + 1;
		String str = " " + month + "-" + year;
		SimpleDateFormat formatter = new SimpleDateFormat("MM-yyyy");
		Long totalDays = null;
		try {
			Date date = formatter.parse(str);
			System.out.println(date);
			Calendar calDate = Calendar.getInstance();
			calDate.setTime(date);
			int totalDaysInt = calDate.getActualMaximum(Calendar.DAY_OF_MONTH);
			totalDays = new Long(totalDaysInt);
			System.out.println(totalDays);

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		return totalDays;

	}

	public static Long getTotalDaysOfDateRange(Timestamp startDate, Timestamp endDate) {
		Long totalDays = new Long(0);
		while (!startDate.after(endDate)) {
			startDate = getDateAfterAdditionOfDays(startDate, new Long(1));
			totalDays++;
		}
		return totalDays;
	}

	public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
		long diffInMillies = date2.getTime() - date1.getTime();
		return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
	}

	public static Timestamp getTodayTimestamp(String hourMinuteValue) {
		hourMinuteValue = hourMinuteValue.concat(":00");
		//System.out.println(hourMinuteValue);
		Timestamp todayTimestamp = Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd ").format(new Date()) // get
				.concat(hourMinuteValue) // and append the time
		);
		return todayTimestamp;
	}

	public static Timestamp getTodayTimestamp() {
		String hourMinuteValue = "00:00:00";
		Timestamp todayTimestamp = Timestamp.valueOf(new SimpleDateFormat("yyyy-MM-dd ").format(new Date()) // get
				.concat(hourMinuteValue) // and append the time
		);
		return todayTimestamp;
	}

	public static String getHourMinuteStr(Timestamp timestamp) {
		String hourMinute = "";
		if (timestamp == null) {
			return null;
		}
		Calendar calDate = Calendar.getInstance();
		calDate.setTime(new Date(timestamp.getTime()));
		// calDate.e
		Integer hour = null;
		Integer minute = null;
		Date date = calDate.getTime();
		hour = date.getHours();
		minute = date.getMinutes();
		return hour.toString() + ":" + minute.toString();
	}

	public static void main(String[] args) {
		Timestamp today = getTodayTimestamp("13:30");
		System.out.println(getHourMinuteStr(today));
	}

}
