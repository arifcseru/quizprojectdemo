package app.vehiclemanagement.businesstier;

import java.sql.Timestamp;

public class UserLoginTO {
	private Integer userLoginId;
	private String userName;
	private String password;
	private Timestamp userCreationDate;

	public Integer getUserLoginId() {
		return userLoginId;
	}

	public void setUserLoginId(Integer userLoginId) {
		this.userLoginId = userLoginId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Timestamp getUserCreationDate() {
		return userCreationDate;
	}

	public void setUserCreationDate(Timestamp userCreationDate) {
		this.userCreationDate = userCreationDate;
	}

	public Timestamp getUserModificationDate() {
		return userModificationDate;
	}

	public void setUserModificationDate(Timestamp userModificationDate) {
		this.userModificationDate = userModificationDate;
	}

	private Timestamp userModificationDate;
}
